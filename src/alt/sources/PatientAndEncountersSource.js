import axios from 'axios';

const PatientAndEncounterSource = {
  fetchPatientWithEncounters: (id) => axios.get(`http://localhost:3000/patients/${id}/encounters`, {
    headers: {
      'Content-Type': 'application/json',
    },
  }),
};

export default PatientAndEncounterSource;
