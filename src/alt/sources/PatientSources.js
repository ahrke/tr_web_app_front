import axios from 'axios';

const PatientSources = {
  fetchPatients: () => axios.get('http://localhost:3000/patients'),
  updatePatient: (id, body) => axios.put(`http://localhost:3000/patients/${id}`, {
    patient: body,
  }),
  createPatient: (body) => axios.post('http://localhost:3000/patients', {
    patient: body,
  }),
  deletePatient: (id) => axios.delete(`http://localhost:3000/patients/${id}`),
};

export default PatientSources;
