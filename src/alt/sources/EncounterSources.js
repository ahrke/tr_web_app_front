import axios from 'axios';

const EncounterSources = {
  updateEncounter: (id, body) => axios.put(`http://localhost:3000/encounters/${id}`, {
    encounter: body,
  }),
  createEncounter: (body) => {
    console.log('from encounterSources, createEncounter. body:', body);
    return axios.post('http://localhost:3000/encounters', {
      encounter: body,
    });
  },
  deleteEncounter: (id) => axios.delete(`http://localhost:3000/encounters/${id}`),
};

export default EncounterSources;
