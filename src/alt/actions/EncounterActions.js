import alt from '../alt'
import EncounterSources from '../sources/EncounterSources'

class EncounterActions {
    updateEncounters = encounters => {
        const encountersObj = encounters.reduce((e, encounter) => {
            e[encounter.id] = encounter
            return e
        }, {})
        return encountersObj
    }

    changeEncounter = encounterId => {
        return encounterId
    }

    changeView = view => {
        return view
    }

    createEncounter = body => {
        return EncounterSources.createEncounter(body)
    }

    updateEncounter = (id, body) => {
        return EncounterSources.updateEncounter(id, body)
    }

    deleteEncounter = id => {
        return EncounterSources.deleteEncounter(id)
    }
}

export default alt.createActions(EncounterActions)