import alt from '../alt';

class MainActions {
  changeContainer(container) {
    return container;
  }
}

export default alt.createActions(MainActions);
