import alt from '../alt';
import PatientSources from '../sources/PatientSources';
import PatientAndEncountersSource from '../sources/PatientAndEncountersSource';

import EncounterActions from './EncounterActions';

class PatientActions {
  changePatient(patient) {
    return patient;
  }

  updatePatients(patients) {
    return patients;
  }

  changeCurrentComponent(comp) {
    return comp;
  }

  fetchPatient(id) {
    PatientAndEncountersSource.fetchPatientWithEncounters(id)
      .then((patientResult) => {
        const { patient } = patientResult.data;
        const { encounters } = patientResult.data;
        this.changePatient(patient);
        EncounterActions.updateEncounters(encounters);
      });
  }

  getAllPatients() {
    PatientSources.fetchPatients()
      .then((patientsResult) => {
        const patients = patientsResult.data.patients.reduce((p, patient) => {
          p[patient.id] = patient;
          return p;
        }, {});
        this.updatePatients(patients);
      });
  }
}

export default alt.createActions(PatientActions);
