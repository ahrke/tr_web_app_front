import alt from '../alt';
import PatientActions from '../actions/PatientActions';

class PatientStore {
  constructor() {
    this.bindListeners({
      onChangePatient: PatientActions.changePatient,
      onUpdatePatients: PatientActions.updatePatients,
      onFetchPatients: PatientActions.getAllPatients,
      onChangeCurrentComponent: PatientActions.changeCurrentComponent,
    });

    this.patient = {
      first_name: 'Not Dave',
    };
    this.patients = null;
    this.appStarted = false;
    this.currentComponent = 'patients';
  }

  onChangeCurrentComponent(comp) {
    this.currentComponent = comp;
  }

  onChangePatient(patient) {
    this.patient = patient;
  }

  onUpdatePatients(patients) {
    this.patients = patients;
    this.appStarted = true;
  }

  onFetchPatient() {
    this.patient = null;
  }

  onFetchPatients() {
    this.patients = null;
  }
}

export default alt.createStore(PatientStore, 'PatientStore');
