import alt from '../alt';
import EncounterActions from '../actions/EncounterActions';

class EncounterStore {
  constructor() {
    this.encounter = null;
    this.encounters = null;
    this.currentView = 'show';

    this.bindListeners({
      onChangeEncounter: EncounterActions.changeEncounter,
      onUpdateEncounters: EncounterActions.updateEncounters,
      onChangeView: EncounterActions.changeView,
    });
  }

  onChangeEncounter(encounterId) {
    this.encounter = this.encounters[encounterId];
  }

  onUpdateEncounters(encounters) {
    this.encounters = encounters;
  }

  onChangeView(view) {
    this.currentView = view;
  }
}

export default alt.createStore(EncounterStore, 'EncounterStore');
