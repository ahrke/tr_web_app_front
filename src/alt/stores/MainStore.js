import alt from '../alt';
import MainActions from '../actions/MainActions';

class MainStore {
  constructor() {
    this.currentContainer = 'patients';

    this.bindListeners({
      onChangeContainer: MainActions.changeContainer,
    });
  }

  onChangeContainer(container) {
    this.currentContainer = container;
  }
}

export default alt.createStore(MainStore, 'MainStore');
