export const tHeaderStyle = {
    borderRadius: '3px',
    fontSize: '1.1em',
    backgroundColor: '#018383',
};

export const tableCellStyle = {
    padding: '3px',
    columnFill: 'auto',
    border: '1px #018383 solid',
    borderRadius: '3px',
    textAlign: 'center',
};

export const tableStyle = {
    padding: '3px',
    columnFill: 'auto',
    border: '1px #018383 solid',
    borderRadius: '3px',
    textSlign: 'center',
};