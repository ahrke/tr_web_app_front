import React from 'react'
import PropTypes from 'prop-types'
import EncounterStore from '../alt/stores/EncounterStore'
import EncounterActions from '../alt/actions/EncounterActions'
import MainActions from '../alt/actions/MainActions'
import connectToStores from 'alt-utils/lib/connectToStores'

import EncounterView from '../components/encounter/EncounterView'
import EncounterUpdateForm from '../components/encounter/EncounterUpdateForm'
import EncounterCreateForm from '../components/encounter/EncounterCreateForm'
import PatientActions from '../alt/actions/PatientActions'

class EncountersContainer extends React.Component {
    static getStores() {
        return [EncounterStore]
    }

    static getPropsFromStores() {
        return EncounterStore.getState()
    }

    toPatientsContainer() {
        MainActions.changeContainer('patients')
    }

    handleRequestResult = result => {
        if (!result.message) {
            PatientActions.fetchPatient(this.props.patient.id)
            MainActions.changeContainer('patients')
        } else {
            console.log("==|==|> received error during request:",result.message)
        }
    }

    handleCreationFormClick = body => {
        EncounterActions.createEncounter(body)
            .then(result => {
                this.handleRequestResult(result)
            })
    }

    handleEditEncounter = body => {
        EncounterActions.updateEncounter(this.props.encounter.id, body)
            .then(result => {
                this.handleRequestResult(result)
            })
    }

    handleDeleteEncounter = () => {
        EncounterActions.deleteEncounter(this.props.encounter.id)
            .then(result => {
                this.handleRequestResult(result)
            })
    }

    render() {
        return (
            <div style={{ "height": "100%" }}>
                {this.props.currentView === 'show' && 
                    <EncounterView 
                        encounter={this.props.encounter} 
                        toPatientsContainer={this.toPatientsContainer} 
                    />}
                {this.props.currentView === 'edit' && 
                    <EncounterUpdateForm 
                        encounter={this.props.encounter} 
                        handleClick={this.handleEditEncounter}
                        patient_id={this.props.patient.id}
                        backToPatient={this.toPatientsContainer}
                    />}
                {this.props.currentView === 'create' && 
                    <EncounterCreateForm 
                        backToPatient={this.toPatientsContainer}
                        handleClick={this.handleCreationFormClick}
                        patient_id={this.props.patient.id}
                        backToPatient={this.toPatientsContainer}
                    />}
            </div>
        )
    }
}

EncountersContainer.propTypes = {
    patient: PropTypes.shape(PropTypes.object).isRequired,
    encounter: PropTypes.func.isRequired,
    currentView: PropTypes.string.isRequired,
}

export default connectToStores(EncountersContainer)