import React from 'react'
import PropTypes from 'prop-types'
import PatientStore from '../alt/stores/PatientStore'
import PatientActions from '../alt/actions/PatientActions'
import PatientSources from '../alt/sources/PatientSources'
import EncounterStore from '../alt/stores/EncounterStore'
import EncounterActions from '../alt/actions/EncounterActions'
import EncounterSources from '../alt/sources/EncounterSources'
import connectToStores from 'alt-utils/lib/connectToStores'

import PatientsView from '../components/patient/PatientsView'
import PatientView from '../components/patient/PatientView'
import PatientUpdateForm from '../components/patient/PatientUpdateForm'
import PatientCreationForm from '../components/patient/PatientCreationForm'
import MainActions from '../alt/actions/MainActions'

class PatientContainer extends React.Component {
    componentDidMount() {
        PatientActions.getAllPatients()
    }

    static getStores() {
        return [PatientStore, EncounterStore]
    }

    static getPropsFromStores() {
        return {
            ...EncounterStore.getState(),
            ...PatientStore.getState()
        } 
            
    }

    handleClick = (id, comp) => {
        PatientActions.fetchPatient(id)
        PatientActions.changeCurrentComponent(comp)
    }

    handleChangeComp = comp => {
        PatientActions.changeCurrentComponent(comp)
    }

    backToAll = () => {
        PatientActions.changeCurrentComponent('patients')
    }
    
    handleRequestResult = result => {
        if (!result.message) {
            PatientActions.getAllPatients()
            PatientActions.changeCurrentComponent('patients')
        } else {
            console.log("==|==|> received error during request:",result.message)
        }
    }   


    handlePatientViewRequestResult = (patientId, result) => {
        if (!result.message) {
            PatientActions.fetchPatient(patientId);
            MainActions.changeContainer('patients');
        } else {
            console.log('==|==|> received error during request:', result.message);
        }
    };

    handleUpdateFormClick = (id, body) => {
        PatientSources.updatePatient(id, body)
            .then(result => {
                this.handleRequestResult(result)
            })
    }

    handleCreationFormClick = body => {
        PatientSources.createPatient(body)
            .then(result => {
                console.log("==|==|> result:", result)
                this.handleRequestResult(result)
            })
            .catch(err => {
                const result = {
                    message: `error creating patient: ${err}`
                }
                this.handleRequestResult(null, result)
            })
    }

    handleDeletePatient = id => {
        PatientSources.deletePatient(id)
            .then(result => {
                this.handleRequestResult(id, result)
            })
    }

    handleDeleteEncounter = (id, encounter, patientId) => {
        EncounterSources.deleteEncounter(id, encounter)
        .then((result) => {
            this.handlePatientViewRequestResult(patientId, result);
        });
    };

    routeToEncounterView = (id, view) => {
        EncounterActions.changeEncounter(id);
        EncounterActions.changeView(view);
        MainActions.changeContainer('encounters');
    };

    addEncounter = () => {
        EncounterActions.changeView('create')
        MainActions.changeContainer('encounters')
    }

    render() {
        return (
            <div style={{ "height": "100%" }}>
                {this.props.currentComponent === 'patients' && 
                    <PatientsView 
                        patients={this.props.patients} 
                        handleClick={this.handleClick} 
                        changeComp={this.handleChangeComp} 
                        handleDeletePatient={this.handleDeletePatient} 
                    />
                }
                {this.props.currentComponent === 'patientCreate' && 
                    <PatientCreationForm 
                        backToAll={this.backToAll} 
                        handleClick={this.handleCreationFormClick} 
                    />
                }
                {this.props.currentComponent === 'patientUpdate' && this.props.patient && 
                    <PatientUpdateForm 
                        patient={this.props.patient} 
                        handleClick={this.handleUpdateFormClick} 
                        backToAll={this.backToAll}
                    />
                }
                {this.props.currentComponent === 'patient' && 
                    <PatientView 
                        patient={this.props.patient} 
                        encounters={this.props.encounters} 
                        appStarted={this.props.appStarted} 
                        backToAll={this.backToAll} 
                        addEncounter={this.addEncounter}
                        handleRequestResult={this.handlePatientViewRequestResult}
                        handleDeleteEncounter={this.handleDeleteEncounter}
                        routeToEncounterView={this.routeToEncounterView}
                    />
                }
            </div>
            
        )
    };
};

PatientContainer.propTypes = {
    appStarted: PropTypes.bool.isRequired,
    patient: PropTypes.shape(PropTypes.object).isRequired,
    encounters: PropTypes.shape(PropTypes.object).isRequired,
    currentComponent: PropTypes.string.isRequired,
};

export default connectToStores(PatientContainer);