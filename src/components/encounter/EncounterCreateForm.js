import React from 'react';
import PropTypes from 'prop-types';
import EncounterForm from './EncounterForm';

const EncounterCreationForm = (props) => (
  <EncounterForm handleClick={props.handleClick} message="create Encounter" patient_id={props.patient_id} backToPatient={props.backToPatient} />
);

EncounterCreationForm.propTypes = {
  handleClick: PropTypes.func.isRequired,
  patient_id: PropTypes.number.isRequired,
  backToPatient: PropTypes.func.isRequired,
};

export default EncounterCreationForm;
