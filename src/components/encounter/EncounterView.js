import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../atoms/Button';
import Table from '../molecules/Table';
import { tHeaderStyle, tableCellStyle, tableStyle } from '../../styles/tableStyles';

const EncounterViewContainer = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    align-items: center;
    padding: 10px;
    background-color: #3a3535;
    color: #f4f4f4;
`;

const tHeaders = [
  'patient id',
  'visit number',
  'admitted at',
  'discharged at',
  'location',
  'room',
  'bed',
];


const EncounterView = ({ toPatientsContainer, encounter }) => {
  const tBody = [[ 
    encounter.patient_id,
    encounter.visit_number,
    encounter.admitted_at,
    encounter.discharged_at,
    encounter.location,
    encounter.room,
    encounter.bed, 
  ]];

  return (
    <EncounterViewContainer>
      <Button onClick={toPatientsContainer}>
        Back
      </Button>
      <Table style={{ margin: "15px" }} headers={tHeaders} data={tBody} tHeaderStyle={tHeaderStyle} tableCellStyle={tableCellStyle} tableStyle={tableStyle} />
    </EncounterViewContainer>
  );
};

EncounterView.propTypes = {
  toPatientsContainer: PropTypes.func.isRequired,
  encounter: PropTypes.shape(PropTypes.object).isRequired,
};

export default EncounterView;
