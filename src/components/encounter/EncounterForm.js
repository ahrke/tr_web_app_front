import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../atoms/Button';

import '../../styles/reactDatetime.css';
import { toPointer } from '../../styles/cursorToPointer';

const EncounterFormContainer = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    align-items: center;
    padding: 20px;
    background-color: #f6e1e1;
    color: #333366;
    font-size: 1.1em;
`;

const EncounterForm = (props) => {
  const {
    patient_id, handleClick, backToPatient, message,
  } = props;
  const enc = props.encounter;

  const [encounter, setEncounter] = useState({
    patient_id,
  });

  useEffect(() => {
    setEncounter({
      ...encounter,
      visit_number: enc ? enc.visit_number : '',
      admitted_at: enc ? enc.admitted_at : '',
      discharged_at: enc ? enc.discharged_at : '',
      location: enc ? enc.location : '',
      room: enc ? enc.room : '',
      bed: enc ? enc.bed : '',
    });
  }, [props]);

  return (
    <EncounterFormContainer>
      <Button styles={{ margin: '15px' }} onClick={backToPatient}>
                back to Patient
      </Button>
      <label htmlFor="visitNumber">
        Visit Number
      </label>
      <input type="text" id="visitNumber" value={encounter.visit_number} onChange={(e) => setEncounter({ ...encounter, visit_number: e.target.value })} />
      <label htmlFor="admittedAt">
        Admitted At
      </label>
      <input type="date" id="admittedAt" value={encounter.admitted_at} onChange={(e) => setEncounter({ ...encounter, admitted_at: e.target.value })} />
      <label htmlFor="dischargedAt">
        Discharged At
      </label>
      <input type="date" id="dischargedAt" value={encounter.discharged_at} onChange={(e) => setEncounter({ ...encounter, discharged_at: e.target.value })} />
      <label htmlFor="location">
        Location
      </label>
      <input type="text" id="location" value={encounter.location} onChange={(e) => setEncounter({ ...encounter, location: e.target.value })} />
      <label htmlFor="room">
        Room
      </label>
      <input type="text" id="room" value={encounter.room} onChange={(e) => setEncounter({ ...encounter, room: e.target.value })} />
      <label htmlFor="bed">
        Bed
      </label>
      <input type="text" id="bed" value={encounter.bed} onChange={(e) => setEncounter({ ...encounter, bed: e.target.value })} />
      <button type="button" onClick={() => handleClick(encounter)} style={{ ...toPointer, borderRadius: '3px', padding: '10px', margin: '10px', backgroundColor: '#7fcd91', color: 'white', fontSize: '1.3em' }}>{message}</button>
      <button onClick={() => backToPatient()} style={ { ...toPointer, borderRadius: '3px', padding: '10px', margin: '10px', backgroundColor: '#f0134d', color: 'white' } }>cancel</button>
    </EncounterFormContainer>
  );
};

EncounterForm.propTypes = {
  handleClick: PropTypes.func.isRequired,
  patient_id: PropTypes.number.isRequired,
  encounter: PropTypes.shape(PropTypes.object).isRequired,
  backToPatient: PropTypes.func.isRequired,
};

export default EncounterForm;
