import React from 'react';
import PropTypes from 'prop-types';
import EncounterForm from './EncounterForm';

const EncounterUpdateForm = ({
  encounter, patient_id, backToPatient, handleClick,
}) => (
  <EncounterForm handleClick={(enc) => handleClick(enc)} encounter={encounter} message="update encounter" patient_id={patient_id} backToPatient={backToPatient} />
);

EncounterUpdateForm.propTypes = {
  encounter: PropTypes.shape(PropTypes.object).isRequired,
  patient_id: PropTypes.number.isRequired,
  backToPatient: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
};

export default EncounterUpdateForm;
