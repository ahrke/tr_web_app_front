import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import TableActions from '../molecules/TableActions';
import Table from '../molecules/Table';

import { tHeaderStyle, tableCellStyle, tableStyle } from '../../styles/tableStyles';

const PTable = styled.div`
    display: flex;
    flex-direction: column;
	margin: 10px;
	width: 80%;
`;

const PatientsTable = ({ patients, handleClick, handleDelete }) => {

  const patientsTableActions = (patient) => {
    const show = {
      handleClick: () => handleClick(patient.id, 'patient'),
      message: 'show',
    };
    const edit = {
      handleClick: () => handleClick(patient.id, 'patientUpdate'),
      message: 'edit',
    };
    const destroy = {
      handleClick: () => handleDelete(patient.id),
      message: 'delete',
    };
    
    return <TableActions show={show} edit={edit} destroy={destroy} />;
  };
  
  const tHeaders = [
    'mrn',
    'first name',
    'last name',
    'actions',
  ];

  const tBody = Object.keys(patients).map((patientId) => {
    const patient = patients[patientId];
    
    return [
      patient.mrn,
      patient.first_name,
      patient.last_name,
      patientsTableActions(patient),
    ];
  });

  return (
    <PTable>
      <Table headers={tHeaders} data={tBody} tHeaderStyle={tHeaderStyle} tableCellStyle={tableCellStyle} tableStyle={tableStyle} />
    </PTable>
  );
};

PatientsTable.propTypes = {
  patients: PropTypes.shape(PropTypes.array).isRequired,
  handleClick: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
};

export default PatientsTable;
