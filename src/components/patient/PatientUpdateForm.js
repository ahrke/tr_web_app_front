import React from 'react';
import PropTypes from 'prop-types';
import PatientForm from './PatientForm';

const PatientUpdateForm = ({ patient, handleClick, backToAll }) => (
  <PatientForm handleClick={(pat) => handleClick(pat.id, patient)} patient={patient} message="update patient" backToPatients={backToAll} />
);

PatientUpdateForm.propTypes = {
  handleClick: PropTypes.func.isRequired,
  patient: PropTypes.shape(PropTypes.object).isRequired,
  backToAll: PropTypes.func.isRequired,
};

export default PatientUpdateForm;
