import React from 'react';
import PropTypes from 'prop-types';
import PatientForm from './PatientForm';

const PatientCreationForm = ({ handleClick, backToAll }) => (
  <PatientForm handleClick={handleClick} message="create patient" backToPatients={backToAll} />
);

PatientCreationForm.propTypes = {
  handleClick: PropTypes.func.isRequired,
  backToAll: PropTypes.func.isRequired,
};

export default PatientCreationForm;
