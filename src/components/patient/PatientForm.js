import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../atoms/Button';

import { toPointer } from '../../styles/cursorToPointer';

const PatientFormContainer = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    align-items: center;
    padding: 20px;
    background-color: #ffd369;
    color: #015668;
    font-size: 1.1em;
`;

const PatientForm = ({
  patient, backToPatients, message, handleClick,
}) => {
  const [p, setP] = useState({});

  useEffect(() => {
    if (patient) {
      setP({
        id: patient.id,
        first_name: patient.first_name,
        middle_name: patient.middle_name,
        last_name: patient.last_name,
        mrn: patient.mrn,
        weight: patient.weight,
        height: patient.height,
      });
    }
  }, [patient]);

  return (
    <PatientFormContainer>
      <Button styles={{ margin: '15px', }, toPointer } className="toPointer" onClick={backToPatients}>
                back to patients
      </Button>
      <label htmlFor="firstName">First Name</label>
      <input type="text" id="firstName" value={p.first_name} onChange={(e) => setP({ ...p, first_name: e.target.value })} />
      <label htmlFor="middleName">Middle Name</label>
      <input type="text" id="middleName" value={p.middle_name} onChange={(e) => setP({ ...p, middle_name: e.target.value })} />
      <label htmlFor="lastName">Last Name</label>
      <input type="text" id="lastName" value={p.last_name} onChange={(e) => setP({ ...p, last_name: e.target.value })} />
      <label htmlFor="mrn">MRN</label>
      <input type="text" id="mrn" value={p.mrn} onChange={(e) => setP({ ...p, mrn: e.target.value })} />
      <label htmlFor="weight">Weight</label>
      <input type="text" id="weight" value={p.weight} onChange={(e) => setP({ ...p, weight: e.target.value })} />
      <label htmlFor="height">Height</label>
      <input type="text" id="height" value={p.height} onChange={(e) => setP({ ...p, height: e.target.value })} />
      <button onClick={() => handleClick(p)} style={ { ...toPointer, borderRadius: '3px', padding: '10px', margin: '10px', backgroundColor: '#7fcd91', color: 'white', fontSize: '1.3em' } }>{message}</button>
      <button onClick={() => backToPatients()} style={ { ...toPointer, borderRadius: '3px', padding: '10px', margin: '10px', backgroundColor: '#f0134d', color: 'white' } }>cancel</button>
    </PatientFormContainer>
  );
};

PatientForm.propTypes = {
  backToPatients: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
  patient: PropTypes.shape(PropTypes.object).isRequired,
};

export default PatientForm;
