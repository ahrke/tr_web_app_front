import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import PatientsTable from './PatientsTable';

const PatientsComponent = styled.div`
  display: flex;
  flex-direction: column;
  justify-context: center;
  align-items: center;
  width: 100%;
  height: 100%;
  background-color: black;
  color: #f9f6f7;
`;

const Loading = styled.div`
  background-color: white;
`;

const AddNewPatientButton = styled.button`
    border: none;
    border-radius: 3px;
    width: 100px;
    padding: 10px;
    height: 50p;
    background-color: #018383;
    color: white;
    font-size: 0.8em;
    justify-self: flex-end;
    cursor: pointer;
`;


const PatientsView = ({
  patients, handleClick, handleDeletePatient, changeComp,
}) => {
  if (!patients) {
    return (
      <Loading>
        ...loading
      </Loading>
    );
  }

  return (
    <PatientsComponent>
      <h2>Patients</h2>
      <AddNewPatientButton onClick={(e) => {
        changeComp('patientCreate');
      }}
      >
        Add New Patient
      </AddNewPatientButton>
      <PatientsTable patients={patients} handleClick={handleClick} handleDelete={handleDeletePatient} />
    </PatientsComponent>
  );
};

PatientsView.propTypes = {
  patients: PropTypes.shape(PropTypes.array).isRequired,
  handleClick: PropTypes.func.isRequired,
  handleDeletePatient: PropTypes.func.isRequired,
  changeComp: PropTypes.func.isRequired,
};

export default PatientsView;
