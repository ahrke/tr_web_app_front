import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import Button from '../atoms/Button';
import Table from '../molecules/Table';
import TableActions from '../molecules/TableActions';

const PatientViewComponent = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    background-color: #ffd369;
    padding: 10px;
    width: 100%;
    height: 100%;
`;

const PatientInfo = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 20px;
    align-items: center;
    background-color: #015668;
    color: #ffd369;
    border-radius: 3px;
    width: 80%;
    margin: 10px;
`;

const PatientView = ({
  addEncounter, patient, backToAll, encounters, appStarted, handleDeleteEncounter, routeToEncounterView
}) => {
  if (!appStarted) {
    backToAll();
    return null;
  } if (!encounters) {
    return (
      <div>
        uh oh....
      </div>
    );
  }

  const theaders = [
    'visit number',
    'arrived at',
    'discharged at',
    'actions',
  ];

  const tbody = Object.keys(encounters).map((encounterId) => {
    const encounter = encounters[encounterId];

    const show = {
      handleClick: () => routeToEncounterView(encounter.id, 'show'),
      message: 'show',
    };
    const edit = {
      handleClick: () => routeToEncounterView(encounter.id, 'edit'),
      message: 'edit',
    };
    const destroy = {
      handleClick: () => handleDeleteEncounter(encounter.id, encounter, patient.id),
      message: 'delete',
    };

    return [
      encounter.visit_number,
      encounter.admitted_at,
      encounter.discharged_at,
      <TableActions show={show} edit={edit} destroy={destroy} />,
    ];
  });

  const tableStyle = {
    backgroundColor: '#263f44',
    color: '#fff1cf',
    padding: '10px',
    borderRadius: '3px',
    marginTop: '15px',
  };

  const tableCellStyle = {
    border: '1px #fff1cf solid',
    padding: '5px',
  };

  return (
    <PatientViewComponent>
      <Button onClick={backToAll}>
                back to all patients
      </Button>
      <PatientInfo>
        <h3>
          {patient.first_name}
          {' '}
          {patient.middle_name}
          {' '}
          {patient.last_name}
        </h3>
                    mrn:
        {' '}
        {patient.mrn}
                    ||  weight:
        {' '}
        {patient.weight}
                    ||  height:
        {' '}
        {patient.height}
        <Button onClick={addEncounter} styles={{ marginTop: '10px' }}>
                    Add encounter
        </Button>
        <Table headers={theaders} data={tbody} tableStyle={tableStyle} tableCellStyle={tableCellStyle} />
      </PatientInfo>
    </PatientViewComponent>
  );
};

PatientView.propTypes = {
  addEncounter: PropTypes.func.isRequired,
  patient: PropTypes.shape(PropTypes.object).isRequired,
  backToAll: PropTypes.func.isRequired,
  encounters: PropTypes.shape(PropTypes.array).isRequired,
  appStarted: PropTypes.bool.isRequired,
  handleDeleteEncounter: PropTypes.func.isRequired,
  routeToEncounterView: PropTypes.func.isRequired,
};

export default PatientView;
