import React from 'react';
import PropTypes from 'prop-types';
import connectToStores from 'alt-utils/lib/connectToStores';
import styled from 'styled-components';
import MainStore from '../alt/stores/MainStore';
import PatientStore from '../alt/stores/PatientStore';
import PatientContainer from '../container/PatientContainer';
import EncountersContainer from '../container/EncountersContainer';

const MainComponent = styled.div`
	height: 100%;
	width: 100%;
`;

class Main extends React.Component {
  static getStores() {
    return [MainStore, PatientStore];
  }

  static getPropsFromStores() {
    return {
      patient: PatientStore.getState().patient,
      ...MainStore.getState(),
    };
  }

  render() {
    const { patient, currentContainer } = this.props;

    return (
      <MainComponent>
        {currentContainer === 'patients' && <PatientContainer />}
        {currentContainer === 'encounters' && <EncountersContainer patient={patient} />}
      </MainComponent>
    );
  }
}

Main.propTypes = {
  currentContainer: PropTypes.string.isRequired,
  patient: PropTypes.shape(PropTypes.object).isRequired,
};

export default connectToStores(Main);
