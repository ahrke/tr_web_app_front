import React from 'react';
import PropTypes from 'prop-types';

const Table = ({
  headers,
  data,
  tableStyle,
  tableCellStyle,
  tHeaderStyle,
  style
}) => (
  <table style={ { ...tableStyle, ...style } }>
    <thead>
      {headers.map((header) => (
        <td key={`h${header}`} style={tHeaderStyle || tableCellStyle}>
          {header}
        </td>
      ))}
    </thead>
    <tbody>
      {data.map((row) => (
        <tr key={`r${row}`}>
          {row.map((item) => <td key={`innerR${item}${Math.random()}`} style={tableCellStyle}>{item}</td>)}
        </tr>
      ))}
    </tbody>
  </table>
);

Table.defaultProps = {
  style: PropTypes.shape(PropTypes.object)
};

Table.propTypes = {
  headers: PropTypes.shape([]).isRequired,
  data: PropTypes.shape(PropTypes.object).isRequired,
  tableStyle: PropTypes.shape(PropTypes.object).isRequired,
  tableCellStyle: PropTypes.shape(PropTypes.object).isRequired,
  tHeaderStyle: PropTypes.shape(PropTypes.object).isRequired,
};

export default Table;
