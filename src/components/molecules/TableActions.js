import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ActionsText = styled.span`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
`;

const ActionItem = styled.span`
	color: #018383;
  padding: 0 10px;
  cursor: pointer;
`;

const actionOption = (obj) => {
  const { handleClick, message } = obj;
  return (
    <ActionItem onClick={(e) => {
		  handleClick();
    }}
    >
      {message}
    </ActionItem>
  );
};

const TableActions = ({ show, edit, destroy }) => (
  <ActionsText>
    {actionOption(show)}
    {actionOption(edit)}
    {actionOption(destroy)}
  </ActionsText>
);

TableActions.propTypes = {
  show: PropTypes.shape(PropTypes.object).isRequired,
  edit: PropTypes.shape(PropTypes.object).isRequired,
  destroy: PropTypes.shape(PropTypes.object).isRequired,
};

export default TableActions;
