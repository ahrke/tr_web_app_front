import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { toPointer } from '../../styles/cursorToPointer';

const ButtonComponent = styled.div`
    border-radius: 3px;
    background-color: #263f44;
    color: #fff1cf;
    padding: 10px;
    width: 50%;
`;

const Button = ({ onClick, styles, children }) => (
  <ButtonComponent onClick={onClick} style={{ ...toPointer, ...styles }}>
    {children}
  </ButtonComponent>
);

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  styles: PropTypes.shape(PropTypes.object).isRequired,
  children: PropTypes.string.isRequired,
};

export default Button;
